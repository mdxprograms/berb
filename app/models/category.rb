class Category < ActiveRecord::Base
  belongs_to :post
  has_many :posts
end
