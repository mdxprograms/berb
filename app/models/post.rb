class Post < ActiveRecord::Base
  belongs_to :user
  has_one :category
  has_many :comments, :dependent => :destroy
  
  scope :category, -> (category_id) { where category_id: category_id }
end
