class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  attr_accessor :login
  has_many :posts, :dependent => :destroy
  has_many :comments, :dependent => :destroy

  validates :username, presence: true
  validates :username, uniqueness: true, if: -> { self.username.present? }
end
