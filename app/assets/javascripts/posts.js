// posts.js
$(document).on('ready page:load', function () {
  var parentHeight = $('.img-responsive').parent().height(),
    videoHeight = $('.post-container iframe').height(),
    postImageHeight = $('.post-image').height();

  // set image on show
  if ($('img').hasClass('post-image')) {
    $('.post-image').css('height', $(window).height() / 1.5);
    $('.post-image').css('width', $(window).width() / 2.5 );
  } else if (videoHeight == null && !$('body').hasClass('show')) {
    $('.img-responsive').height('200px');
  } else {
    // set image to height of video on index
    $('.img-responsive').height(videoHeight);
  }
  
  // set video view size on show
  if ($('iframe').hasClass('large-video')) {
    setVideoSize();
    $(window).resize(function() {
      setVideoSize();
    });
  }
   
  // show correct fields for posting
  if ($('#new-post').hasClass('new-post')) {
    showOnly();
  } else {
    if ($('.video-field :input').val() != "") {
      $('.video-field').fadeIn();
    } else {
      $('.image-field').fadeIn();
    }
    $('.post-title').fadeIn();
    $('.post-submit').fadeIn();
  }
});

// functions
function setVideoSize() {
  if ($(this).width() > 768) {
    $('iframe').removeClass('col-sm-12');
    $('.large-video').attr('height', $(window).height() / 1.5);
    $('.large-video').attr('width', $(window).width() / 1.5 );
  } else {
    $('.large-video').attr('height', $(window).height() / 2.2);
    $('iframe').addClass('col-sm-12');
  }
}

function showOnly() {
  $('.image-field :input').on('keyup', function() {
    if ($(this).val() != "") {
      $('.add-video').fadeOut('fast');
    } else {
      $('.add-video').fadeIn('fast');
    }
  });
  
  $('.video-field :input').on('keyup', function() {
    if ($(this).val() != "") {
      $('.add-image').fadeOut('fast');
    } else {
      $('.add-image').fadeIn('fast');
    }
  });
}

function addImage() {
  $('.video-field').fadeOut('fast', function () {
    $('.image-field').fadeIn('fast');
    $('.post-title').fadeIn('fast');
    $('.post-submit').fadeIn('fast');
    $('.category-field').fadeIn('fast');
    
    $('.image-field :input').prop('required', true);
    $('.video-field :input').prop('required', false);
  });
}

function addVideo() {
  $('.image-field').fadeOut('fast', function () {
    $('.video-field').fadeIn('fast');
    $('.post-title').fadeIn('fast');
    $('.post-submit').fadeIn('fast');
    $('.category-field').fadeIn('fast');
    
    $('.video-field :input').prop('required', true);
    $('.image-field :input').prop('required', false);
  });
}


