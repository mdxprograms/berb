(function($) {
  $(document).on('ready', function () {
    // toggle chat window on click
    $('.chat-button').on('click', toggleChat);
    
    // show tooltip for chat window keypress
    $('.chat-button').on('mouseenter', function(event) {
      $(this).attr('title', 'You can also use the esc key to toggle chat window');
    });
    
    // toggle chat window on 'esc' keypress
    Mousetrap.bind('esc', function(e) {
      toggleChat();
      return false;
    });

    // use firebase for data store
    var chatDB = new Firebase('https://berb.firebaseio.com/');
    $('#chat-message').keypress(function (e) {
      if (e.keyCode == 13) {
        var name = window.username;
        var text = $('#chat-message').val();
        chatDB.push({name: name, text: text});
        $('#chat-message').val('');
      }
    });
    chatDB.on('child_added', function(snapshot) {
      var message = snapshot.val();
      displayChatMessage(message.name, message.text);
    });
    function displayChatMessage(name, text) {
      $('<div/>').text(text)
        .css('padding', '10px 2px 2px 10px')
        .prepend($('<em/>').text(name+': ').css('color', 'cyan')).appendTo($('.chat-area'));
      $('.chat-button').addClass('btn-info');
      $('.chat-area').scrollTop($('.chat-area').prop("scrollHeight"));
    };
  }); // end document ready

  // toggle chat
  var toggleChat = function(e) {
    $('.chat-button').removeClass('btn-info');
    $('.chat-button').toggleClass('warp-button btn-danger');
    $('.chat-button span').toggleClass('fa-weixin fa-close');
    $('.chat-area, #chat-message').fadeToggle('slow').toggleClass('show-chat');
    $('.chat-area').scrollTop($('.chat-area').prop("scrollHeight"));
  };
})(jQuery);