class WelcomeController < ApplicationController
  before_action :authenticate_user!

  def index
    if current_user.username != nil
      @username = current_user.username
    else current_user.email
      @username = current_user.email
    end
    
    # get user data for dashboard
    @user = User.find(current_user.id)
    @posts = @user.posts
  end
end
