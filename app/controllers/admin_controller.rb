class AdminController < ApplicationController
  before_filter :authenticate_user!
  before_filter :is_admin
  layout 'admin'

  def is_admin
    unless current_user.admin?
      redirect_to '/'
    end
  end
end
