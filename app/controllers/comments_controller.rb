class CommentsController < ApplicationController
  def create
    @post = Post.find(params[:post_id])
    @comment = @post.comments.create!({:body => params[:comment][:body], :user_id => current_user.id})
    redirect_to @post
  end
  
  def show
    
  end
  
  private
  
  def comment_params
    params.require(:comment).permit(:body, :user_id)
  end
end
