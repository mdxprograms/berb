class Admin::UsersController < AdminController
  load_and_authorize_resource

  def index
  	@users = User.where.not(id: current_user.id)
  end

  def new
    @user = User.new
  end

  def create
    @user = User.create(user_params)
    if @user.save
      flash[:notice] = 'Successfully created user'
      redirect_to '/admin'
    else
      render 'new'
    end
  end

  def edit
    @user = User.find_by_id(params[:id])
  end

  def show
    @user = User.find(params[:id])

    respond_to do |format|
      format.html
    end
  end

  def update
    @user = User.find_by_id(params[:id])

    if @user.update(user_params)
      flash[:notice] = 'Successfully updated user'
      redirect_to '/admin/users'
    else
      flash[:alert] = "Unable to update user"
      render 'edit'
    end
  end

  def destroy
    @user = User.find_by_id(params[:id])
    if @user.destroy
      flash[:notice] = "Successfully deleted #{@user.username}"
      redirect_to '/admin/users'
    else
      flash[:alert] = "#{@user.email} could not be removed."
      render_to '/admin/users'
    end
  end


  private

  def user_params
    params.require(:user).permit(:email, :username, :password, :password_confirmation, :admin)
  end
end
