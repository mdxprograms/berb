class Admin::PostsController < AdminController
  load_and_authorize_resource
  
  def index
    @posts = Post.all
  end
  
  def new
    @user = User.new
  end

  def create
    @post = Post.create(user_params)
    if @post.save
      flash[:notice] = "Successfully created #{@post.title}"
      redirect_to '/admin/posts'
    else
      render 'new'
    end
  end

  def edit
    @post = Post.find_by_id(params[:id])
  end

  def show
    @post = Post.find(params[:id])

    respond_to do |format|
      format.html
    end
  end

  def update
    @post = Post.find_by_id(params[:id])

    if @post.update(post_params)
      flash[:notice] = "Successfully updated #{@post.title}"
      redirect_to '/admin/posts'
    else
      flash[:alert] = "Unable to update #{@post.title}"
      render 'edit'
    end
  end
  
  def destroy
    @post = Post.find_by_id(params[:id])
    if @post.destroy
      flash[:notice] = "Successfully deleted #{@post.title}"
      redirect_to '/admin/posts'
    else
      flash[:alert] = "#{@post.title} could not be removed."
      render_to '/admin/posts'
    end
  end
  
  private

  def post_params
    params.require(:post).permit(:title, :image, :video, :category_id)
  end
end
