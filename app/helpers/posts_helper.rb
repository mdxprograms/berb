module PostsHelper
  require 'open-uri'
  
  def embed(url)
    # youtube url
    # http://youtu.be/E3mFLiiNDMQ
    if url.include? "youtube"
      regex = /youtube.com.*(?:\/|v=)([^&$]+)/
      video_id = url.match(regex)[1]
      return content_tag(:iframe,
                         nil,
                         src: "//www.youtube.com/embed/#{video_id}",
                         height: '200px')
    elsif url.include? "youtu.be"
      uri = URI(url)
      return content_tag(:iframe,
                         nil,
                         src: "//www.youtube.com/embed/#{uri.path[1..12]}",
                         height: '200px')

    else
      return content_tag(:iframe,
                         nil,
                         src: url)
    end
  end
  
  def lg_embed(url)
    # youtube url
    if url.include? "youtube"
      regex = /youtube.com.*(?:\/|v=)([^&$]+)/
      video_id = url.match(regex)[1]
      return content_tag(:iframe,
                         nil,
                         src: "//www.youtube.com/embed/#{video_id}",
                         class: "large-video")
    elsif url.include? "youtu.be"
      uri = URI(url)
      return content_tag(:iframe,
                         nil,
                         src: "//www.youtube.com/embed/#{uri.path[1..12]}",
                         class: "large-video")
    else
      return content_tag(:iframe,
                         nil,
                         src: url)
    end
  end
end
