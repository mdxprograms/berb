Rails.application.routes.draw do
  resources :comments

  # root
  root 'welcome#index'

  # use devise for users
  devise_for :users

  # resources
  resources :posts do
    resources :comments, :only => [:create]
  end

  # session routes
  devise_scope :user do
    get "/login" => "devise/sessions#new"
    get "/logout" => "devise/sessions#destroy"
  end

  # admin routes
  get '/admin' => 'admin/users#index'
  namespace :admin do
    resources :users
    resources :posts
  end


  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
