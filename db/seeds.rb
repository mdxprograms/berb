# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
if !User.exists?(:email => 'mdx.programs@gmail.com')
  user = User.new
  user.email = 'mdx.programs@gmail.com'
  user.username = 'mdxprograms'
  user.password = 'JaW420791083'
  user.admin = true
  user.save!
end

categories = ['music', 'comedy', 'sports', 'gif', 'bicycles', 'superheroes', 'computers', 'movies']

categories.each do |cat|
  category = Category.new
  category.name = cat
  category.save!
end