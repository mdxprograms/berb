class ModifyPosts < ActiveRecord::Migration
  def self.up
    remove_column :posts, :body
    add_column :posts, :image, :string
    add_column :posts, :video, :string
  end

  def self.down
    add_column :posts, :body, :text
    remove_column :posts, :image
    remove_column :posts, :video
  end
end
